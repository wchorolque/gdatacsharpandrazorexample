﻿using System;

namespace GenerateHTMLPageFromGoogleSpreadSheet
{
    public class Circunscripcion
    {
        public Circunscripcion()
        {
        }

        public string Circ
        {
            get;
            set;
        }

        public decimal PDC
        {
            get;
            set;
        }

        public decimal PVBIEP
        {
            get;
            set;
        }

        public decimal MSM
        {
            get;
            set;
        }

        public decimal MASICPC
        {
            get;
            set;
        }

        public decimal UD
        {
            get;
            set;
        }

        public override string ToString()
        {
            return string.Format("[Circunscripcion: Circ={0}, PDC={1}, PVBIEP={2}, MSM={3}, MASICPC={4}, UD={5}]", Circ, PDC, PVBIEP, MSM, MASICPC, UD);
        }
    }
}

