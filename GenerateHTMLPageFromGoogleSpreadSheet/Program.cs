﻿namespace GenerateHTMLPageFromGoogleSpreadSheet
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Google.GData.Client;
    using Google.GData.Extensions;
    using Google.GData.Spreadsheets;
    using System.Linq;
    using NDesk.Options;

    //http://www.hanselman.com/blog/NuGetPackagesOfTheWeek12AccessingGoogleSpreadsheetsWithGDataFromCAndHostingRazorTemplatesToGenerateHTMLFromAConsoleApp.aspx
    class MainClass
    {
        public static void Main(string[] args)
        {
            string user = null;
            string pass = null;
            OptionSet options = new OptionSet();
            options.Add("u|user=", "Usuario", v => user = v);
            options.Add("p|pass=", "Password", v => pass = v);

            try
            {
                options.Parse(args);
            }
            catch (OptionException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            if (null == user || null == pass)
            {
                Console.WriteLine("Debe proporcionar el usuario y password de la cuenta");
                return;
            }

            SpreadsheetsService ssService = new SpreadsheetsService("my-cool-app");
            ssService.setUserCredentials(user + "@gmail.com", pass);
            // Get the spreadsheet
            SpreadsheetQuery query = new SpreadsheetQuery();
            SpreadsheetFeed feed = ssService.Query(query);

            var spreadSheet = (from x in feed.Entries
                                        where x.Title.Text.Contains("Resultados al 2014/10/17 - 20:08")
                                        select x).First();

            // Get the first worksheet from that sheet
            AtomLink link = spreadSheet.Links.FindService(GDataSpreadsheetsNameTable.WorksheetRel, null);
            WorksheetQuery wsQuery = new WorksheetQuery(link.HRef.ToString());
            WorksheetFeed wsFeed = ssService.Query(wsQuery);

            var sheet = wsFeed.Entries.First();

            // Get the cells
            AtomLink cellFeedLink = sheet.Links.FindService(GDataSpreadsheetsNameTable.CellRel, null);
            CellQuery cellQuery = new CellQuery(cellFeedLink.HRef.ToString());
            CellFeed cellFeed = ssService.Query(cellQuery);

            List<Circunscripcion> circunscripciones = new List<Circunscripcion>();

            uint lastRow = 1;
            Circunscripcion circ = new Circunscripcion();
            foreach (CellEntry currentCell in cellFeed.Entries)
            {
                if (currentCell.Cell.Row == 1)
                {
                    continue;
                }

                if (currentCell.Cell.Row > lastRow && lastRow != 1)
                {
                    circunscripciones.Add(circ);
                    circ = new Circunscripcion();
                }

                switch (currentCell.Cell.Column)
                {
                    case 1: 
                        circ.Circ = currentCell.Cell.Value;
                        break;
                    case 2:
                        circ.PDC = decimal.Parse(currentCell.Cell.Value);
                        break;
                    case 3:
                        circ.PVBIEP = decimal.Parse(currentCell.Cell.Value);
                        break;
                    case 4:
                        circ.MSM = decimal.Parse(currentCell.Cell.Value);
                        break;
                    case 5:
                        circ.MASICPC = decimal.Parse(currentCell.Cell.Value);
                        break;
                    case 6:
                        circ.UD = decimal.Parse(currentCell.Cell.Value);
                        break;
                }

                lastRow = currentCell.Cell.Row;
            }

            foreach (var c in circunscripciones)
            {
                Console.WriteLine(c);
            }

            string template = File.ReadAllText("Template.html");
            string result = RazorEngine.Razor.Parse(template, circunscripciones);
            File.WriteAllText("Resultado.html", result);
        }
    }
}
